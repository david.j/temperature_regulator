/*
 * defaults.h
 *
 *  Created on: Sep 15, 2018
 *      Author: Dave
 */

#ifndef INC_DEFAULTS_H_
#define INC_DEFAULTS_H_

#define T_REF_DEF				12800 /* 40,00�C */
#define P_MAX_DEF				6000 	/*  8,000W */
#define CURR_KI_DEF			600
#define PWM_MIN_DEF			10		/*    3,9% */
#define PWM_MAX_DEF			246		/*   96,1% */
#define TEMP_KP_DEF			5000
#define TEMP_KI_DEF			100
#define TEMP_KD_DEF			12000
#define TEMP_KAW_DEF		100
#define TEMP_KDL_DEF		100
#define R_HEAT_DEF			1920	/*  30,00R */
#define G_SYS_DEF				32727	/* 1/32,04R */
#define T_ZEN_A_DEF			557
#define T_ZEN_B_DEF			30137
#define T_ZEN_C_DEF			-1249
#define I_HEAT_ZERO_DEF	416
#define I_HEAT_GAIN_DEF	56586
#define V_CAP_GAIN_DEF	32386
#define T_AMB_ZERO_DEF	0
#define T_AMB_GAIN_DEF	0
#define DT_STB_LIM_DEF	64
#define STATUSLED_DEF		0x21
#define DEFAULT_ST_DEF	0

#endif /* INC_DEFAULTS_H_ */
