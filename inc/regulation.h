/*
 * regulation.h
 *
 *  Created on: Dec 27, 2017
 *      Author: Dave
 */

#ifndef INC_REGULATION_H_
#define INC_REGULATION_H_

#include <defs.h>

extern void currReg_Reset(void);
extern void tempReg_Reset(s16 meas);
extern u16 currReg_Run(s16 ref, s16 meas);
extern u16 tempReg_Run(s16 ref, s16 meas);

#endif /* INC_REGULATION_H_ */
