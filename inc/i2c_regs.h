/*
 * i2c_regs.h
 *
 *  Created on: Sep 10, 2018
 *      Author: Dave
 */

#ifndef INC_I2C_REGS_H_
#define INC_I2C_REGS_H_

#include <defs.h>

#define REG_NUM			0x45
#define WRITE_START	0x19

extern u8 *code ptrList[];

#endif /* INC_I2C_REGS_H_ */
