/*
 * variables.h
 *
 *  Created on: Aug 22, 2018
 *      Author: Dave
 */

#ifndef INC_VARIABLES_H_
#define INC_VARIABLES_H_

#include <defs.h>

#define PWMreg PCA0CPH0

typedef struct
{
	/* Current regulator */
	u16 curr_Ki;			/* Integrative coefficient. */
	u16 curr_OutMin;	/* Maximum allowed output. */
	u16 curr_OutMax;	/* Minimum allowed output. */
	/* Temperature regulator */
	u16 temp_Kp;			/* Proportional coefficient. */
	u16 temp_Ki;			/* Integrative coefficient. */
	u16 temp_Kd;			/* Derivative coefficient. */
	u8	temp_Kaw;			/* Antiwindup coefficient. */
	u8	temp_Kdl;			/* Derivative LPF coefficient. */
} reg_par_t;

typedef struct
{
	u16 V_cap_gain;		/* Capacitor voltage scaling */
	s16 I_heat_zero;	/* Heater current zero offset */
	u16 I_heat_gain;		/* Heater current scale */
	s16 T_amb_zero;		/* Ambient temperature zero offset */
	u16 T_amb_gain;		/* Ambient temperature scale */
	s16 T_zen_A;				/* Zener temperature square coefficient */
	s16 T_zen_B;				/* Zener temperature linear coefficient */
	u16 T_zen_C;				/* Zener temperautre zero offset */
} cal_const_t;

/* Measured values */
extern s16 idata T_zen;	/* Zener temperature. */
extern s16 idata T_amb;	/* Ambient temperature. */
extern s16 idata I_heat;	/* Heater current. */
extern s16 idata V_cap;	/* Voltage on input capacitor. */

/* Calculated values */
extern s16 idata P_meas;	/* Measured heater power (via heater current). */
extern s16 idata P_ref;	/* Reference heater power - temperature reg output. */
extern s16 idata P_lim;	/* Maximum attainable heater power (calculated from V_cap). */

/* Reference value */
extern s16 idata T_ref;	/* Target temperature. */
extern u8 idata PWM_forced;
extern u16 idata Power_forced;

/* System settings */
extern s16 idata P_max;	/* Maximum allowed heater power - hard limit */
extern reg_par_t idata reg_par;	/* Regulation parameters */
extern cal_const_t idata cal_const;	/* Calibration constants */
extern u16 idata R_heat;									/* Heater resistance */
extern u16 idata G_sys;						/* Total system conductance */
extern u8 idata PWM;
extern u8 idata PWM_min;
extern u8 idata PWM_max;
extern u16 idata dT_stable_lim;
extern u16 idata statusLED_config;

/* System state */
extern u8 bdata status;
extern u8 bdata config;
extern u8 bdata default_state;
extern u8 bdata errors;

/* Readback diagnostics */
extern u16 idata Iheat_ADC;
extern u16 idata Tzen_ADCf;
extern u16 idata Vcap_ADCf;
extern u16 idata Tamb_ADCf;

#endif /* INC_VARIABLES_H_ */
