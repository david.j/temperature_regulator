/*
 * defs.h
 *
 *  Created on: Dec 26, 2017
 *      Author: Dave
 */

#include <SI_EFM8BB1_Register_Enums.h>

#ifndef INC_DEFS_H_
#define INC_DEFS_H_

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t

#define s8 int8_t
#define s16 int16_t
#define s32 int32_t

#define f32 float

#define LEDgreen 				(1<<1)
#define LEDred 					(1<<0)
#define Trigger					(1<<4)
#define Enable					(1<<6)

#define pin_set(pin)		P1 |= pin
#define pin_clear(pin)	P1 &= ~pin

#define GreenON()				pin_set(LEDgreen)		// Turn green LED on
#define GreenOFF()			pin_clear(LEDgreen)	// Turn green LED off
#define RedON()					pin_set(LEDred)			// Turn red LED on
#define RedOFF()				pin_clear(LEDred)		// Turn red LED off

#define PWMEnable()			P1 |= Enable
#define PWMDisable()		P1 &= ~Enable

#define DEC_BITS 				8									// decimal places for fixed-point arithmetic
#define ROUND 					(1<<(DEC_BITS-1))	// addition for rounding

#endif /* INC_DEFS_H_ */
