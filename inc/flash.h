/*
 * flash.h
 *
 *  Created on: Sep 11, 2018
 *      Author: Dave
 */

#ifndef INC_FLASH_H_
#define INC_FLASH_H_

#include <defs.h>

#define MEMORY_ADDR 0x1600
#define DATA_LEN		43

/* Fetch data from flash location */
#define Flash_ByteRead(index) (*(((u8 code *)MEMORY_ADDR) + index))

extern void Flash_MemoryErase(void);
extern void Flash_ByteWrite(u8 index, u8 byte);

#endif /* INC_FLASH_H_ */
