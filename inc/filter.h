/******************************************************************************
*	filter.h
*
*	Header file of filter.c.
*
*	Author: David Jereb
******************************************************************************/

#ifndef INC_FILTER_H_
#define INC_FILTER_H_

#include <defs.h>

void Filter_Init(void);
u16 Filter_Sample(u16 rawIn, u8 chan);

#endif /* INC_FILTER_H_ */
