/******************************************************************************
*	scaling.h
*
*	Header file of scaling.c.
*
*	Author: David Jereb
******************************************************************************/

#ifndef INC_SCALING_H_
#define INC_SCALING_H_

#include <defs.h>

s16 Scale_T_amb(u16 rawIn);
s16 Scale_V_cap(u16 rawIn);
s16 Scale_T_zen(u16 rawIn);
s16 Scale_I_heat(u16 rawIn);

#endif /* INC_SCALING_H_ */
