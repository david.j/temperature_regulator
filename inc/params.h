/*
 * params.h
 *
 *  Created on: Sep 11, 2018
 *      Author: Dave
 */

#ifndef INC_PARAMS_H_
#define INC_PARAMS_H_

#include <defs.h>

#define MEM_INIT 0x11

extern void Params_Init(void);
extern void Params_Defaults(void);
extern void Params_Store(void);
extern void Params_Load(void);
extern u8 Params_Check(void);

#endif /* INC_PARAMS_H_ */
