/*
 * utils.h
 *
 *  Created on: Jan 1, 2018
 *      Author: Dave
 */

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

s16 currentToPower(s16 current);
s16 voltageToPower(s16 voltage);
u8 sarSqrt(u16 number);

void temp2str(s16 in, u8 *buffer);
void tempraw2str(u16 in, u8 *buffer);

#endif /* INC_UTILS_H_ */
