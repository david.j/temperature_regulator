/*
 * adc.h
 *
 *  Created on: Dec 27, 2017
 *      Author: Dave
 */
#include <defs.h>

#ifndef INC_ADC_H_
#define INC_ADC_H_

#define adcRead() (u16)ADC0
#define adcConvReady() (ADC0CN0 & ADC0CN0_ADINT__BMASK)

static const u8 adInput[] = { ADC0MX_ADC0MX__ADC0P6, ADC0MX_ADC0MX__ADC0P7, ADC0MX_ADC0MX__ADC0P2, ADC0MX_ADC0MX__ADC0P3 };

typedef enum adChans { TambSense = 0, VcapSense = 1, TzenSense = 2, IheatSense = 3 };



#endif /* INC_ADC_H_ */
