## Temperature Regulator

This is the firmware for my ovenized voltage standard temperature regulator. The idea is to keep the reference zener diodes heated to a temperature higher than ambient and maintain that temperature stable regardless of ambient temperature fluctuations.
This technique, in theory, should entirely eliminate voltage errors caused by temperature fluctuations of the zener diodes.

The code is written for an EFM8BB10F8G microcontroller (8051 core). It is stripped down to a bare minimum level of abstraction to avoid unnecessary subroutine calls (every clock cycle counts!), therefore might be difficult to port to a different platform.