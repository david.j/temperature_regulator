/*
 * params.c
 *
 *  Created on: Sep 11, 2018
 *      Author: Dave
 */

#include <defs.h>
#include <params.h>
#include <flash.h>
#include <i2c_regs.h>
#include <variables.h>
#include <defaults.h>

SI_LOCATED_VARIABLE_NO_INIT(flash_reserved_page[512], u8, const code, MEMORY_ADDR);

void Params_Init(void)
{
	if(Flash_ByteRead(0) == MEM_INIT)
	{
		/* Memory initialized, read them. */
		Params_Load();
	}
	else
	{
		/* Memory not initialized, initialize defaults. */
		Params_Defaults();
	}
}

void Params_Defaults(void)
{
	T_ref = T_REF_DEF;
	P_max = P_MAX_DEF;
	reg_par.curr_Ki = CURR_KI_DEF;
	PWM_min = PWM_MIN_DEF;
	PWM_max = PWM_MAX_DEF;
	reg_par.temp_Kp = TEMP_KP_DEF;
	reg_par.temp_Ki = TEMP_KI_DEF;
	reg_par.temp_Kd = TEMP_KD_DEF;
	reg_par.temp_Kaw = TEMP_KAW_DEF;
	reg_par.temp_Kdl = TEMP_KDL_DEF;
	R_heat = R_HEAT_DEF;
	G_sys = G_SYS_DEF;
	cal_const.T_zen_A = T_ZEN_A_DEF;
	cal_const.T_zen_B = T_ZEN_B_DEF;
	cal_const.T_zen_C = T_ZEN_C_DEF;
	cal_const.I_heat_zero = I_HEAT_ZERO_DEF;
	cal_const.I_heat_gain = I_HEAT_GAIN_DEF;
	cal_const.V_cap_gain = V_CAP_GAIN_DEF;
	cal_const.T_amb_zero = T_AMB_ZERO_DEF;
	cal_const.T_amb_gain = T_AMB_GAIN_DEF;
	dT_stable_lim = DT_STB_LIM_DEF;
	statusLED_config = STATUSLED_DEF;
	default_state = DEFAULT_ST_DEF;

	Params_Store();
}

void Params_Load(void)
{
	u8 index, byte;

	/* Reset the CRC generator */
	CRC0CN0 |= CRC0CN0_CRCINIT__INIT;

	/* First byte is just the initialization value, only used for CRC */
	CRC0IN = Flash_ByteRead(0);

	for(index = 1; index < (REG_NUM - WRITE_START - 3); index++)
	{
		/* Read each byte and write it to its respective register */
		byte = Flash_ByteRead(index);
		*ptrList[index+WRITE_START] = byte;
		/* Also do CRC on the value */
		CRC0IN = byte;
	}

	/* End CRC generation */
	CRC0CN0 &= ~CRC0CN0_CRCPNT__BMASK;

	/* Left bracket executed first (lower CRC byte), then right (higher byte) */
	if((Flash_ByteRead(index++) == CRC0DAT) && (Flash_ByteRead(index) == CRC0DAT))
	{
		errors &= ~(0x80);
	}
	else
	{
		errors |= 0x80;
	}
}

void Params_Store(void)
{
	u8 index, byte;
	/* Erase the paramter memory page */
	Flash_MemoryErase();

	/* Reset the CRC generator */
	CRC0CN0 |= CRC0CN0_CRCINIT__INIT;

	/* First byte set to 0x11, this is for the initial check of parameter
	 * memory initialization */
	Flash_ByteWrite(0, MEM_INIT);

	/* Run every byte through CRC */
	CRC0IN = MEM_INIT;

	/* Store every register *after* config */
	for(index = 1; index < (REG_NUM - WRITE_START - 3); index++)
	{
		byte = *ptrList[index+WRITE_START];
		Flash_ByteWrite(index, byte);
		CRC0IN = byte;
	}
  /* End CRC generation */
	CRC0CN0 &= ~CRC0CN0_CRCPNT__BMASK;

	/* Write CRC values into memory, first lower then higher byte */
  Flash_ByteWrite(index++, CRC0DAT);
  Flash_ByteWrite(index, CRC0DAT);
}

u8 Params_Check(void)
{


	return 1;
}
