/*
 * smbus.c
 *
 *  Created on: Aug 23, 2018
 *      Author: Dave
 */

#include <defs.h>
#include <smbus.h>
//#include <variables.h>
#include <i2c_regs.h>

s8 addr = -1;

SI_INTERRUPT(SMBUS0_ISR, SMBUS0_IRQn)
{
	u8 idata temp;

	if (SMB0CN0_ARBLOST == 0)
	{
		switch (SMB0CN0 & 0xF0)          // Decode the SMBus status vector
		{
			// Slave Receiver: Start+Address received
			case SMB_SRADD:
				SMB0CN0_STA = 0;           // Clear SMB0CN0_STA bit

				if ((SMB0DAT & 0x01) == READ) // If the transfer is a master READ,
				{
					// Slave read mode - prepare data to transmit
					// Prepare outgoing byte
					if(addr < 0)
					{
						addr = 0;
					}

					SMB0DAT = *((u8 data *)ptrList[addr]);

					addr++;
				}
				else
				{
					// Slave write mode - data will be received
					if(addr < 0)
					{
						addr = WRITE_START;
					}
				}
				SMB0CN0_ACK = 1;	/* ACK received data */
				break;

			// Slave Receiver: Data received
			case SMB_SRDB:
				// Store received data
				temp = SMB0DAT;

				if(addr == WRITE_START) /* Writing to config register */
				{
					if(temp & 0x80) /* Received register address */
					{
						temp = temp & 0x7F;
						if(temp < REG_NUM) /* Register address valid */
						{
							addr = (s8)temp;
							SMB0CN0_ACK = 1;	/* ACK received data */
						}
						else /* Register address invalid */
						{
							SMB0CN0_ACK = 0;	/* NACK received data */
						}
					}
					else /* Received configuration setting */
					{
						config_set = temp;
						SMB0CN0_ACK = 1;
					}
				}
				else if((addr >= WRITE_START) && (addr < REG_NUM))
				{
					*(ptrList[addr]) = temp;
					addr++;
					SMB0CN0_ACK = 1;		/* ACK received data */
				}
				else
				{
					SMB0CN0_ACK = 0;		/* NACK received data */
				}
				break;

			// Slave Receiver: Stop received while either a Slave Receiver or
			// Slave Transmitter
			case SMB_SRSTO:
				addr = -1;
				SMB0CN0_STO = 0;          // SMB0CN0_STO must be cleared by software when
																	// a STOP is detected as a slave
				break;

			// Slave Transmitter: Data byte transmitted
			case  SMB_STDB:
				if (SMB0CN0_ACK == 1)      // If Master SMB0CN0_ACK's, send the next byte
				{
					// Prepare next outgoing byte
					if(addr < REG_NUM)
					{
						SMB0DAT = *(ptrList[addr]);
						addr++;
					}
					else
					{
						/* Invalid address */
						SMB0DAT = 0xFE;
					}
				}                          // Otherwise, do nothing
				break;

			// Slave Transmitter: Arbitration lost, Stop detected
			//
			// This state will only be entered on a bus error condition.
			// In normal operation, the slave is no longer sending data or has
			// data pending when a STOP is received from the master, so the SMB0CN0_TXMODE
			// bit is cleared and the slave goes to the SRSTO state.
			case SMB_STSTO:
				addr = -1;
				SMB0CN0_STO = 0;          // SMB0CN0_STO must be cleared by software when
																	// a STOP is detected as a slave
				break;

			// Default: all other cases undefined
			default:
				addr = -1;
				SMB0CF &= ~0x80;           // Reset communication
				SMB0CF |= 0x80;
				SMB0CN0_STA = 0;
				SMB0CN0_STO = 0;
				SMB0CN0_ACK = 1;
				break;
		}
	}
	// SMB0CN0_ARBLOST = 1, Abort failed transfer
	else
	{
		addr = -1;
		SMB0CN0_STA = 0;
		SMB0CN0_STO = 0;
		SMB0CN0_ACK = 1;
	}

	SMB0CN0_SI = 0;                     // Clear SMBus interrupt flag
}

SI_INTERRUPT (TIMER3_ISR, TIMER3_IRQn)
{

	SMB0CF  &= ~0x80;                   // Disable SMBus
	SMB0CF  |=  0x80;                   // Re-enable SMBus
	addr = -1;

	TMR3CN0 &= ~TMR3CN0_TF3H__BMASK;		// Clear interrupt flag.
}
