/*
 * uart.c
 *
 *  Created on: Jan 27, 2018
 *      Author: Dave
 */

#include <defs.h>

extern u8 send;
extern u8 xdata txCursor;
extern u8 xdata txBuf[25];
extern u8 send;


SI_INTERRUPT (UART0_ISR, UART0_IRQn)
{
	u8 receive, key;

	if(SCON0_RI == 1) /* receive interrupt */
	{
		SCON0_RI = 0; /* Clear interrupt flag */
		receive = SBUF0;
	}
	if(SCON0_TI == 1) /* transmit interrupt */
	{
		SCON0_TI = 0; /* Clear interrupt flag */
		key = txBuf[txCursor++];
		if((txCursor < 25) && (key != 0))
		{
			SBUF0 = key;
		}
		else
		{
			txCursor = 0;
			send = 0;
		}
	}
}
