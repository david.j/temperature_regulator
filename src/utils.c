/*
 * utils.c
 *
 *  Created on: Dec 31, 2017
 *      Author: Dave
 */

#include <defs.h>
#include <utils.h>
#include <adc.h>
/*#include <pidParams.h>*/
#include <variables.h>

/******************************************************************************
*	s16 currentToPower(s16 current)
*******************************************************************************
*	Function calculates the heater power corresponding to the current flowing
*	through it.
*
*	Parameters:
*	current - heater current input variable [mA * 2^4]
*
*	Return:
*	heater power [mW]
******************************************************************************/
s16 currentToPower(s16 current)
{
	u32 temp;

	// ALL FIXED-POINT NUMBES ARE RIGHT-JUSTIFIED!

	// Current is in 10.4 fixed-point format.
	// Square the current.
	// One of the factors needs to be cast as s32 to yield a s32 result.
	temp = (s32)current * current;

	temp += 0x0080;

	// The result of the multiplication is 20.8 fixed-point.
	// Clip the 8 LSB to get whole mA^2 value.
	// This is essentially temp = temp>>8, but Keil is stupid.
	// Squaring guarantees a positive result, therefore MS byte can be set to 0.
	*(((u8 *) &temp)+3) = *(((u8 *) &temp)+2);
	*(((u8 *) &temp)+2) = *(((u8 *) &temp)+1);
	*(((u8 *) &temp)+1) = *((u8 *) &temp);
	*((u8 *) &temp) = 0;

	// Multiply by heater resistance.
	// Rheat is in 6.6 fixed-point format, scaled by 1.024 (!NOT 1024!).
	// Result is in uW in 26.6 fixed-point format.
	temp *= R_heat;

	temp += 0x8000;

	// Clip the bottom 6 bits to get a whole value plus
	// 10 additional bits to convert directly to mW.
	// 16 LSB total are clipped from the above result.
	// This is the reason for having Rheat scaled by 1.024 earlier.
	return *((s16 *) &temp);
}

/******************************************************************************
*	s16 voltageToPower(s16 voltage)
*******************************************************************************
*	Function calculates the maximum heater power corresponding to the voltage
*	present on the input bulk capacitors.
*
*	Parameters:
*	voltage - capacitor voltage [mV]
*
*	Return:
*	heater power [mW]
******************************************************************************/
s16 voltageToPower(s16 voltage)
{
	u32 temp;

	/* Voltage is in 16.0 fixed-point format.
	 * Multiply the voltage with the inverse of the power system resistance to
	 * obtain the maximum heater current. */
	temp = (s32)voltage * G_sys;

	/* Add rounding offset */
	temp += 0x8000;

	/* Pass the calculated current to the currentToPower function, which
	 * calculates the maximum attainable heater power.
	 * The 16 LSbits are clipped to obtain current in the correct format. */
	return currentToPower(*((u16 *) &temp));
}

/******************************************************************************
*	u8 sarSqrt(u16 number)
*******************************************************************************
*	Function calculates the square root of the provided number.
*
*	Parameters:
*	number - number to calculate the square root of
*
*	Return:
*	square root of provided number
******************************************************************************/
u8 sarSqrt(u16 number)
{
	// Prepare the variables for the calculation. Result to zero, highest bit set
	// in weight.
	u8 result = 0x00;
	u16 square;
	u8 weight = 0x80;

	while(weight)	// Repeat while weight is higher than zero.
	{
		result += weight; // Add weight to result.
		square = result * result; // Calculate the square of the current result.
		if(square > number)	// Check is the square exceeds the input number.
		{
			result -= weight; // Clear the bit if the square does exceed the number.
		}
		weight >>= 1; // Move to the next lower bit.
	}

	// Return the calculated square root value.
	return result;
}

void temp2str(s16 in, u8 *buffer)
{
	s32 strtemp = in;
	u8 cnt = 0;
	u8 digit;
	if(strtemp < 0)
	{
		*buffer++ = '-';
		strtemp = -strtemp;
	}
	else
	{
		*buffer++ = '+';
	}
	while(cnt < 6)
	{
		digit = '0';
		while(strtemp > 25600)
		{
			digit++;
			strtemp -= 25600;
		}
		strtemp *= 10;
		*buffer++ = digit;
		if(++cnt == 3)
		{
			*buffer++ = ',';
		}
	}
	*buffer++ = '�';
	*buffer++ = 'C';
	*buffer++ = '\r';
	*buffer++ = '\n';
	*buffer = 0;
}

void tempraw2str(u16 in, u8 *buffer)
{
	s32 temp = in;
	u8 cnt = 0;
	u8 digit;
	while(cnt < 5)
	{
		digit = '0';
		while(temp > 10000)
		{
			digit++;
			temp -= 10000;
		}
		temp *= 10;
		*buffer++ = digit;
		cnt++;
	}
	*buffer++ = ' ';
}
