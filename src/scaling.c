/******************************************************************************
*	scaling.c
*
*	Scaling functions for transforming ADC readings to useful units.
*
*	Author: David Jereb
******************************************************************************/

#include <defs.h>
#include <scaling.h>
#include <variables.h>

/*==================== Functions ============================================*/

/******************************************************************************
*	s16 Scale_T_amb(u16 rawIn)
*******************************************************************************
*	Scale filtered ADC ambient temperature readings to �C (8.8 fixed-point) using
*	a linear function.
*
*	Parameters:
*	rawIn - raw ADC input value
*
*	Return:
*	scaled value
******************************************************************************/
s16 Scale_T_amb(u16 rawIn)
{
	u32 temp;

	/* Subtract the zero offset from the input value */
	temp = (u32)rawIn - cal_const.T_amb_zero;
	/* Multiply temp by the scaling coefficient */
	temp *= cal_const.T_amb_gain;

	/* Divide by 2^16 and return the value (division done by clipping the bottom
	 * two bytes from the result) */
	return *((s16 *) &temp);
}

/******************************************************************************
*	s16 Scale_V_cap(u16 rawIn)
*******************************************************************************
*	Scale the filtered ADC capacitor voltage readings to mV (16.0 fixed-point)
*	using a linear function.
*
*	Parameters:
*	rawIn - raw ADC input value
*
*	Return:
*	scaled value
******************************************************************************/
s16 Scale_V_cap(u16 rawIn)
{
	u32 temp;

	/* Multiply the input value with the scale */
	temp = (u32)rawIn * cal_const.V_cap_gain;

	/* Divide by 2^16 and return the value (division done by clipping the bottom
	 * two bytes from the result) */
	return *((s16 *) &temp);
}

/******************************************************************************
*	s16 Scale_T_zen(u16 rawIn)
*******************************************************************************
*	Scale the filtered ADC zener temperature readings to �C (8.8 fixed-point)
*	using a second degree polynomial function.
*
*	Parameters:
*	rawIn - raw ADC input value
*
*	Return:
*	scaled value
******************************************************************************/
s16 Scale_T_zen(u16 rawIn)
{
	u32 temp;
	s16 result;

	/* Initialize result with zero offset coefficient */
	result = cal_const.T_zen_C;

	/* Multiply input value with linear coefficient */
	temp = (u32)rawIn * cal_const.T_zen_B;
	/* Divide by 2^16 and add to result (take just the top two bytes)*/
	result += (*((s16 *) &temp));

	/* Calculate the square of the input value */
	temp = (u32)rawIn * rawIn;
	/* Divide by 2^16 */
	temp = (u32)(*((u16 *) &temp));
	/* Multiply with the square coefficient */
	temp *= cal_const.T_zen_A;
	/* Divide by 2^16 again and add to result */
	result += (*((s16 *) &temp));

	return result;
}

/******************************************************************************
*	s16 Scale_I_heat(u16 rawIn)
*******************************************************************************
*	Scale the raw ADC heater current readings to mA (10.4 fixed-point,
*	right-justified) using a linear function.
*
*	Parameters:
*	rawIn - raw ADC input value
*
*	Return:
*	scaled value
******************************************************************************/
s16 Scale_I_heat(u16 rawIn)
{
	u32 temp;

	/* Subtract the zero offset from the input value */
	temp = (u32)rawIn - cal_const.I_heat_zero;
	/* Multiply temp by the scaling coefficient */
	temp *= cal_const.I_heat_gain;

	/* Clip the 16 LSbits of the result to attain the current in mA
	 * (10.4 fixed-point format, right-justified) */
	return *((s16 *) &temp);
}
