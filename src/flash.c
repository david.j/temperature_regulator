/*
 * flash.c
 *
 *  Created on: Sep 11, 2018
 *      Author: Dave
 */

#include <defs.h>
#include <flash.h>

void Flash_MemoryErase(void)
{
	/* Disable interrupts */
	IE &= ~(0x80);

	/* Unlock the flash */
	FLKEY = 0xA5;
	FLKEY = 0xF1;

	/* Set PSEE & PSWE bits */
	PSCTL = 0x03;

	/* Write a single byte to the target page to initiate page erasure */
	*((u8 xdata *)MEMORY_ADDR) = 0x00;

	/* Clear PSEE & PSWE bits */
	PSCTL = 0x00;

	/* Enable interrupts */
	IE |= 0x80;
}

extern void Flash_ByteWrite(u8 index, u8 byte)
{
	/* Disable interrupts */
	IE &= ~(0x80);

	/* Unlock the flash */
	FLKEY = 0xA5;
	FLKEY = 0xF1;

	/* Set PSWE bits */
	PSCTL = 0x01;

	/* Write a single byte to the target page to initiate page erasure */
	*(((u8 xdata *)MEMORY_ADDR) + index) = byte;

	/* Clear PSEE & PSWE bits */
	PSCTL = 0x00;

	/* Enable interrupts */
	IE |= 0x80;
}
