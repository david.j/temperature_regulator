/*
 * i2c_regs.c
 *
 *  Created on: Sep 10, 2018
 *      Author: Dave
 */

#include <defs.h>
#include <i2c_regs.h>
#include <variables.h>

u8 *code ptrList[] = \
{ &status,														&errors,\
	(u8 idata *)&T_zen,									((u8 idata *)&T_zen)+1,\
	(u8 idata *)&T_amb,									((u8 idata *)&T_amb)+1,\
	(u8 idata *)&P_ref,									((u8 idata *)&P_ref)+1,\
	(u8 idata *)&I_heat,								((u8 idata *)&I_heat)+1,\
	(u8 idata *)&V_cap,									((u8 idata *)&V_cap)+1,\
	(u8 idata *)&P_meas,								((u8 idata *)&P_meas)+1,\
	(u8 idata *)&P_lim,									((u8 idata *)&P_lim)+1,\
	&PWM,																(u8 idata *)&Iheat_ADC,\
	((u8 idata *)&Iheat_ADC)+1,					(u8 idata *)&Tzen_ADCf,\
	((u8 idata *)&Tzen_ADCf)+1,					(u8 idata *)&Vcap_ADCf,\
	((u8 idata *)&Vcap_ADCf)+1,					(u8 idata *)&Tamb_ADCf,\
	((u8 idata *)&Tamb_ADCf)+1,					&config,\
	(u8 idata *)&T_ref,									((u8 idata *)&T_ref)+1,\
	(u8 idata *)&P_max,									((u8 idata *)&P_max)+1,\
	(u8 idata *)&reg_par.curr_Ki, 			((u8 idata *)&reg_par.curr_Ki)+1,\
	&PWM_min,														&PWM_max,\
	(u8 idata *)&reg_par.temp_Kp,				((u8 idata *)&reg_par.temp_Kp)+1,\
	(u8 idata *)&reg_par.temp_Ki,				((u8 idata *)&reg_par.temp_Ki)+1,\
	(u8 idata *)&reg_par.temp_Kd,				((u8 idata *)&reg_par.temp_Kd)+1,\
	&reg_par.temp_Kaw,									&reg_par.temp_Kdl,\
	(u8 idata *)&R_heat,								((u8 idata *)&R_heat)+1,\
	(u8 idata *)&G_sys,									((u8 idata *)&G_sys)+1,\
	(u8 idata *)&cal_const.T_zen_A,			((u8 idata *)&cal_const.T_zen_A)+1,\
	(u8 idata *)&cal_const.T_zen_B,			((u8 idata *)&cal_const.T_zen_B)+1,\
	(u8 idata *)&cal_const.T_zen_C,			((u8 idata *)&cal_const.T_zen_C)+1,\
	(u8 idata *)&cal_const.I_heat_zero,	((u8 idata *)&cal_const.I_heat_zero)+1,\
	(u8 idata *)&cal_const.I_heat_gain,	((u8 idata *)&cal_const.I_heat_gain)+1,\
	(u8 idata *)&cal_const.V_cap_gain,	((u8 idata *)&cal_const.V_cap_gain)+1,\
	(u8 idata *)&cal_const.T_amb_zero,	((u8 idata *)&cal_const.T_amb_zero)+1,\
	(u8 idata *)&cal_const.T_amb_gain,	((u8 idata *)&cal_const.T_amb_gain)+1,\
	&PWM_forced,												(u8 idata *)&dT_stable_lim,\
	((u8 idata *)&dT_stable_lim)+1,			&statusLED_config,\
	&default_state,											(u8 idata *)&Power_forced,\
	((u8 idata *)&Power_forced)+1 };

//u8 code allowWrite[] = { 0x03, 0x42, 0x19, 0x03, 0x90 };
//u8 code allowMask[] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

/* The upper bits of address select the byte to read,
 * the lower bits select which bit to check */
//#define write_allowed(x) (singlebyte[(x >> 3)] & singlemask[(x & 0x07)])
