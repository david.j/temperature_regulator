/******************************************************************************
*	filter.c
*
*	Filter functions for smoothing ADC measurements.
*
*	Author: David Jereb
******************************************************************************/

#include <defs.h>
#include <filter.h>

/*==================== Local variables ======================================*/

static u32 filtStore[3][2]; /* Previous filter output samples */
static u16 rawStore[3][2];	/* Previous filter input samples */

/*==================== Functions ============================================*/

/******************************************************************************
*	initInputFilter()
*******************************************************************************
*	Function initializes the input filter internal variables. It is necessary to
*	call this function before using the inputFilter() function!
******************************************************************************/
void Filter_Init(void)
{
	u8 i;
	// Clear the old sample buffer for every channel.
	for(i = 0; i < 3; i++)
	{
		filtStore[i][0] = 0;
		filtStore[i][1] = 0;
		rawStore[i][0] = 0;
		rawStore[i][1] = 0;
	}
}

/******************************************************************************
*	u16 inputFilter(u16 rawIn, u8 chan)
*******************************************************************************
*	Low-pass filtering of ADC raw data. ADC returns a 14-bit result, the filter
*	has gain of 4, so the end result is a 16-bit unsigned value.
*	Bandwidth: 7.1Hz @ 1kSps
*
*	Parameters:
*	rawIn - raw input value
*	chan - channel number
*
*	Return:
*	filtered value
******************************************************************************/
u16 Filter_Sample(u16 rawIn, u8 chan)
{
	u32 data filtTemp;

	/*               z^2 + 2 z + 1
	*  H(z) = ---------------------------
	*          z^2 - 482/256 z + 227/256
	*  Gain: 1024
	*  End result is divided by 256, leaving a gain of 4. */
	filtTemp  = (u32)482 * filtStore[chan][0];
	filtTemp -= (u32)227 * filtStore[chan][1];
	filtTemp += (u32)256 * rawIn;
	filtTemp += (u32)512 * rawStore[chan][0];
	filtTemp += (u32)256 * rawStore[chan][1];
	filtTemp += ROUND;

	// Clip the 8 LSB to divide by 256, ROUND has been added to the sum above.
	// This is essentially temp = temp>>8, but Keil is stupid.
	*(((u8 *) &filtTemp)+3) = *(((u8 *) &filtTemp)+2);
	*(((u8 *) &filtTemp)+2) = *(((u8 *) &filtTemp)+1);
	*(((u8 *) &filtTemp)+1) = *((u8 *) &filtTemp);
	*((u8 *) &filtTemp) = 0;

	// Store current samples and push the older samples backwards.
	filtStore[chan][1] = filtStore[chan][0];
	filtStore[chan][0] = filtTemp;
	rawStore[chan][1] = rawStore[chan][0];
	rawStore[chan][0] = rawIn;

	/* Return only the middle two bytes of the 32-bit integer, essentially
	 * dividing by 256 */
	return *((u16 *)(((u8 *) &filtTemp)+1));
}
