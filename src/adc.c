/******************************************************************************
*	adc.c
*
*	Procedures related to the ADC (conversion complete
*
*	Author: David Jereb
******************************************************************************/

#include <defs.h>
#include <adc.h>
#include <variables.h>
#include <filter.h>
#include <scaling.h>
#include <utils.h>
#include <regulation.h>

enum adChans adChan = TambSense;
u8 prescaler = 0;
extern u8 send;

SI_INTERRUPT (TIMER2_ISR, TIMER2_IRQn)
{
	/* Select next ADC channel */
	ADC0MX = adInput[adChan];
	/* Start the conversion */
	ADC0CN0 |= ADC0CN0_ADBUSY__BMASK;
	/* Clear interrupt flag */
	TMR2CN0 &= ~TMR2CN0_TF2H__BMASK;
}

SI_INTERRUPT (ADC0EOC_ISR, ADC0EOC_IRQn)
{
	u16 temp;
	/* Take ADC reading from registers */
	temp = adcRead();

	switch(adChan)
	{
		case TambSense:
			/* Filter measured ambient temperature ADC value */
			Tamb_ADCf = Filter_Sample(temp, TambSense);
			/* Scale the filtered value to �C */
			T_amb = Scale_T_amb(Tamb_ADCf);

			/* TODO Add configuration state machine here */
			break;

		case VcapSense:
			/* Filter measured capacitor voltage ADC value */
			Vcap_ADCf = Filter_Sample(temp, VcapSense);
			/* Scale the filtered value to mV */
			V_cap = Scale_V_cap(Vcap_ADCf);
			/* Calculate the maximum attainable heater power */
			P_lim = voltageToPower(V_cap);
			/* Cap the maximum output power if it exceeds the hard limit */
			if(P_lim > P_max)
			{
				P_lim = P_max;
			}
			break;

		case TzenSense:
			/* Filter measured zener temperature ADC value */
			Tzen_ADCf = Filter_Sample(temp, TzenSense);
			/* Run every 100th cycle */
			if (++prescaler >= 100)
			{
				prescaler = 0;
				/* Scale the temperature to �C */
				T_zen = Scale_T_zen(Tzen_ADCf);
				if(send == 0)
				{
					send = 1;
				}
				if(config != 0)
				{
					/* Execute the temperature regulation loop */
					P_ref = tempReg_Run(T_ref, T_zen);
				}
				else
				{
					P_ref = 0;
					tempReg_Reset(T_zen);
				}
			}
			break;

		case IheatSense:
			Iheat_ADC = temp;
			/* Scale the measured heater current to mA */
			I_heat = Scale_I_heat(temp);
			/* Calculate the current heater power */
			P_meas = currentToPower(I_heat);
			if(config != 0)
			{
				/* Execute the current regulation loop */
				temp = currReg_Run(P_ref, P_meas);
				/* Set the square root of PID output to PWM output register */
				PWM = sarSqrt(temp);
				PWMEnable();
				PWMreg = PWM;
			}
			else
			{
				PWMDisable();
				PWMreg = 0;
				currReg_Reset();
			}
			break;
	}

	/* Cycle to next ADC channel */
	adChan++;
	/* Clip the upper bits, keep the channels 0-3 */
	adChan &= 0x03;

	/* Clear interrupt flag */
	ADC0CN0 &= ~ADC0CN0_ADINT__BMASK;
}
