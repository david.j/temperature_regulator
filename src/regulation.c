/*
 * regulation.c
 *
 *  Created on: Dec 27, 2017
 *      Author: Dave
 */
#include <defs.h>
#include <regulation.h>
#include <variables.h>

s32 data curr_Int;	/* Integration variable for current regulator */
s32 data temp_Diff;
s16 data measOld;
s32 data outSat;
s32 data temp_Int;
s16 data powerMax;

void currReg_Reset(void)
{
	curr_Int = 0;
}

void tempReg_Reset(s16 meas)
{
	measOld = meas;
	outSat = 0;
	temp_Int = 0;
	temp_Diff = 0;
	GreenOFF();
}

u16 currReg_Run(s16 ref, s16 meas)
{
	s32 data temp;
	u16 data retVal;

	/* Calculate the error value. */
	temp = (s32)ref - meas;

	if((temp <= (s32)dT_stable_lim) && (temp >= (s32)(-dT_stable_lim)))
	{
		GreenON();
	}
	else
	{
		GreenOFF();
	}

	/* Integrate the error. */
	curr_Int = curr_Int + temp;

	/* Limit integration to a reasonable value range. In reality, the integrator
	 * should never use the whole range. */
	curr_Int = (curr_Int > 20000000) ? 20000000 : curr_Int;
	curr_Int = (curr_Int < -5000000) ? -5000000 : curr_Int;

	/* Multiply integrator value by coefficient Ki. */
	temp = reg_par.curr_Ki * curr_Int + ROUND;

	/* Divide by 2^8. Only the lower 3 bytes are important, MSbyte is ignored. */
	*(((u8 *) &temp)+3) = *(((u8 *) &temp)+2);
	*(((u8 *) &temp)+2) = *(((u8 *) &temp)+1);
	*(((u8 *) &temp)+1) = *( (u8 *) &temp);

	/* Check the most significant bit (sign) */
	if(*((u8 *) &(temp)) & 0x80)
	{
		/* Result negative, return zero. */
		retVal = 0;
	}
	else
	{
		/* Result positive, check if it exceeds the maximum. */
		if(temp > (s32)reg_par.curr_OutMax)
		{
			/* Result exceeds max, cap the output. */
			retVal = reg_par.curr_OutMax;
		}
		else
		{
			/* Result within limits, copy the bottom two bytes
			 * to the return variable. */
			retVal = (u16)temp;

			/* Check if output is below minimum permitted value. */
			if(retVal < reg_par.curr_OutMin)
			{
				/* Compare to half the minimum permitted value. */
				if(retVal < (reg_par.curr_OutMin >> 1))
				{
					/* Below half set the output to zero. */
					retVal = 0;
				}
				else
				{
					/* Above half set it to minimum. */
					retVal = reg_par.curr_OutMin;
				}
			}
		}
	}
	/* Return final result. */
	return retVal;
}


u16 tempReg_Run(s16 ref, s16 meas)
{
	s32 data temp;
	u16 data retVal;
	s32 data error;

	/* Error calculation *********************************************/
	error = (s32)ref - meas;

	/* Integration with anti-windup **********************************/
	temp = reg_par.temp_Kaw * outSat + ROUND;

	*(((u8 *) &temp)+3) = *(((u8 *) &temp)+2);
	*(((u8 *) &temp)+2) = *(((u8 *) &temp)+1);
	*(((u8 *) &temp)+1) = *((u8 *) &temp);
	*((u8 *) &temp) = (*((u8 *) &temp) & 0x80) ? 0xFF : 0x00;

	temp_Int = temp_Int + error - temp;

	temp_Int = (temp_Int > 1000000) ? 1000000 : temp_Int;
	temp_Int = (temp_Int < -200000) ? -200000 : temp_Int;

	/* Derivative calculation ****************************************/
	temp = reg_par.temp_Kdl * ((s32)meas - measOld - temp_Diff) + ROUND;

	*(((u8 *) &temp)+3) = *(((u8 *) &temp)+2);
	*(((u8 *) &temp)+2) = *(((u8 *) &temp)+1);
	*(((u8 *) &temp)+1) = *((u8 *) &temp);
	*((u8 *) &temp) = (*((u8 *) &temp) & 0x80) ? 0xFF : 0x00;

	temp_Diff += temp;

	measOld = meas;

	/* Combining all three *******************************************/
	temp = reg_par.temp_Kp * error + reg_par.temp_Ki * temp_Int - reg_par.temp_Kd * temp_Diff + ROUND;

	*(((u8 *) &temp)+3) = *(((u8 *) &temp)+2);
	*(((u8 *) &temp)+2) = *(((u8 *) &temp)+1);
	*(((u8 *) &temp)+1) = *((u8 *) &temp);
	*((u8 *) &temp) = (*((u8 *) &temp) & 0x80) ? 0xFF : 0x00;

	if(*((u8 *) &(temp)) & 0x80)
	{
		/* Result negative, zero output. */
		retVal = 0;
	}
	else
	{
		if(temp > (s32)P_lim)
		{
			retVal = P_lim;
		}
		else
		{
			retVal = (u16)temp;
		}
	}

	outSat = temp - retVal;

	return retVal;
}
