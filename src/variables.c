/*
 * variables.c
 *
 *  Created on: Aug 22, 2018
 *      Author: Dave
 */

#include <defs.h>
#include <variables.h>

/******** Measured values ****************************************************/
s16 idata T_zen;	/* Zener temperature */
s16 idata T_amb;	/* Ambient temperature */
s16 idata I_heat;	/* Heater current */
s16 idata V_cap;	/* Voltage on input capacitor */

/******** Calculated values **************************************************/
s16 idata P_meas;	/* Measured heater power (via heater current) */
s16 idata P_ref;	/* Reference heater power - temperature reg output */
s16 idata P_lim;	/* Maximum attainable heater power (calculated from V_cap) */

/******** Reference value ****************************************************/
s16 idata T_ref;	/* Target temperature */
u8 idata PWM_forced;
u16 idata Power_forced;

/******** System settings ****************************************************/
s16 idata P_max;							/* Maximum allowed heater power - hard limit */
reg_par_t idata reg_par;			/* Regulation parameters */
cal_const_t idata cal_const;	/* Calibration constants */
u16 idata R_heat;						/* Heater resistance */
u16 idata G_sys;							/* Total system conductance */
u8 idata PWM;
u8 idata PWM_min;						/* Minimum permitted PWM level */
u8 idata PWM_max;						/* Maximum permitted PWM level */
u16 idata dT_stable_lim;
u16 idata statusLED_config;

/******** System state *******************************************************/
u8 bdata status = 0x00;
u8 bdata config = 0x00;
u8 bdata config_set;
u8 bdata default_state = 0x00;
u8 bdata errors = 0x00;

/******** Readback diagnostics ***********************************************/
u16 idata Iheat_ADC;
u16 idata Tzen_ADCf;
u16 idata Vcap_ADCf;
u16 idata Tamb_ADCf;
